﻿using MediatR;

namespace TraineeAspApp.CQRS
{
    public interface ICommand<out T> : IRequest<T>
    {
    }
}