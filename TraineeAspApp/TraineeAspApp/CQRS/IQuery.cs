﻿using MediatR;

namespace TraineeAspApp.CQRS
{
    public interface IQuery<out T> : IRequest<T>
    {
    }
}