﻿using Npgsql;

namespace TraineeAspApp.Database
{
    public interface IDatabaseContext
    {
        NpgsqlConnection CreateConnection();
    }
}