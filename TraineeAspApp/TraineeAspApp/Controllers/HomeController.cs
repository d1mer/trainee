﻿using Microsoft.AspNetCore.Mvc;

namespace TraineeAspApp.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}