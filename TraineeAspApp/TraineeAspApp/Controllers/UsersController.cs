﻿using System;
using System.Threading.Tasks;
using LS.Helpers.Hosting.API;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using TraineeAspApp.Commands.Users.RegistryUser;

namespace TraineeAspApp.Controllers
{
    // <inheritdoc />
    [SwaggerTag("Users")]
    [Route("api/users")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class UsersController : Controller
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [SwaggerOperation("Registry user")]
        [Produces("application/json", "application/xml")]
        [HttpPost]
        [Route("Registry")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ExecutionResult> RegistryUser([FromBody] RegistryUserCommand registryUserCommand)
        {
            ExecutionResult result = await _mediator.Send(registryUserCommand);

            return result;
        }
    }
}
