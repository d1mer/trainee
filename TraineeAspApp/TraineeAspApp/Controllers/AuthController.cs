﻿using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace TraineeAspApp.Controllers
{
    // <inheritdoc />
    [SwaggerTag("Auth")]
    [Route("api/auth")]
    [ApiExplorerSettings(GroupName = "v1")]
    public class AuthController : Controller
    {
        private readonly IMediator _mediator;

        public AuthController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        //[SwaggerOperation("Authorize user")]
        //[Produces("application/json", "application/xml")]
        //[HttpPost]
        //[Route("login")]
        //[ProducesResponseType(typeof(ActionResult<LoginQueryResult>), 200)]
        //public async Task<IActionResult> Login([FromBody] LoginQuery loginQuery)
        //{
        //    var result = await _mediator
        //        .Send(loginQuery);

        //    return this.FromExecutionResult(result);
        //}
    }
}