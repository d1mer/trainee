﻿using System;
using Microsoft.AspNetCore.Identity;

namespace TraineeAspApp.Models
{
    public class User : IdentityUser
    {
        public string FullName { get; set; }

        public string Password { get; set; }
    }
}
