﻿using System;
using System.ComponentModel.DataAnnotations;
using LS.Helpers.Hosting.API;
using Microsoft.AspNetCore.Mvc;
using TraineeAspApp.CQRS;

namespace TraineeAspApp.Commands.Users.RegistryUser
{
    public sealed class RegistryUserCommand : ICommand<ExecutionResult>
    {
        [Required]
        [StringLength(40, ErrorMessage = "Name length can't be more than 40.")]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords mismatch")]
        [Display(Name = "Confirm password")]
        public string PasswordComfirm { get; set; }
    }
}
