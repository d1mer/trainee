﻿using System;
using System.Threading;
using System.Threading.Tasks;
using LS.Helpers.Hosting.API;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TraineeAspApp.Database;
using TraineeAspApp.Models;

namespace TraineeAspApp.Commands.Users.RegistryUser
{
    public class RegistryUserCommandHandler : IRequestHandler<RegistryUserCommand, ExecutionResult>
    {
        private readonly UserManager<User> _userManager;
        RoleManager<IdentityRole> _roleManager;
        private readonly DatabaseContext _dbContext;
        private readonly IMediator _mediator;

        public RegistryUserCommandHandler(UserManager<User> userManager,
            RoleManager<IdentityRole> roleManager,
            DatabaseContext dbContext,
            IMediator mediator)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _dbContext = dbContext;
            _mediator = mediator;
        }

        public async Task<ExecutionResult> Handle(RegistryUserCommand request, CancellationToken cancellationToken)
        {
            ExecutionResult result = null;

            using (var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken))
            {
                try
                {
                    var existUser = _dbContext.Users
                        .Include(u => u.Email)
                        .FirstOrDefaultAsync(u => u.Email == request.Email);

                    if (existUser == null)
                    {
                        var user = new User
                        {
                            FullName = request.FullName.Trim(),
                            Email = request.Email.Trim(),
                            PasswordHash = request.Password.Trim().GetHashCode().ToString(),
                        };

                        _dbContext.Users.Add(user);
                    }
                    else
                    {
                        result = new ExecutionResult(new ErrorInfo("Email already taken"));
                    }
                }
                catch (Exception ex)
                {
                    var message = $"Error while execution {nameof(RegistryUserCommand)}";
                    result = new ExecutionResult(new ErrorInfo(message));
                }
            }

            return result;
        }
    }
}
